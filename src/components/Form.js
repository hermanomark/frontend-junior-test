import React, { Component } from "react";
import Counter from "./Counter";
import './Form.css';

class Form extends Component {
  state = {
    counter: '',
    counters: []
  }

  addCounterHandler = (event) => {
    event.preventDefault();
    let counters = [ ...this.state.counters ];
    counters.push({title: this.state.counter, count: 0});
    this.setState({counters: counters});
    this.setState({counter: ''});
  }

  deleteCounterHandler = (index) => {
    let counters = [...this.state.counters];
    counters.splice(index, 1);
    this.setState({counters: counters})
  }

  onChangeHandler = (index, value) => {
    let counters = [...this.state.counters];
    counters.map((counter, i) => {
      return i === index ? counter.count = value : counter;
    });
    this.setState({counters: counters});
  }

  render() {
    const counters = this.state.counters.map((counter, index) => {
       return (<div key={index} className="counters">
                <button 
                  className="button is-primary" 
                  onClick={() => this.deleteCounterHandler(index)}>Delete</button>
                <span>{counter.title}</span>
                <Counter
                  type="number"
                  placeholder="0"
                  value={counter.count}
                  onChange={(event) => this.onChangeHandler(index, parseInt(event.target.value))}/>
              </div>);
    });

    const totalCount = this.state.counters.reduce((a,b) => a + b.count, 0);

    return (
      <React.Fragment>
        <h1 align="center" className="title is-5">Sitepoint Junior FrontEnd Test by Mark Hermano</h1>
        <form onSubmit={this.addCounterHandler}>
          <label className="label" htmlFor="name">
            Counter Name
          </label>
          <input 
            className="input" 
            type="text" 
            placeholder="Counter Name"
            value={this.state.counter}
            onChange={(event) => this.setState({counter: event.target.value})} />
          <button className="button is-primary">Add</button>
        </form>
        {counters}
        <h1 className="title is-5">Total Count: {totalCount}</h1>
      </React.Fragment>
    );
  }
}

export default Form;
