import React, { Component } from "react";
import "./Counter.css";

class Counter extends Component {
  render() {
    return (<React.Fragment>
              <input 
                className="input my-input"
                type={this.props.type} 
                placeholder={this.props.placeholder}
                value={this.props.value}
                onChange={this.props.onChange} />
            </React.Fragment>);
  }
}

export default Counter;